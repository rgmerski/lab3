tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

// template(name={$e},deklaracje={$d}) "<deklaracje;separator=\" \n\"> start: <name;separator=\" \n\"> ";

@members {
  Integer numer = 0;
  Integer iter = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($ID.text)}? -> setVar(p1={$i1.text},p2={$e2.st})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ID {globals.hasSymbol($ID.text)}?                       -> getVar(p1={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr) {iter++;} -> 
        if_statement(condition={$e1.st}, positive={$e2.st}, negative={$e3.st}, iter={iter.toString()})
    ;
    